package paquete;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import paquete.Lineaorden;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2014-11-21T23:59:20")
@StaticMetamodel(Producto.class)
public class Producto_ { 

    public static volatile SingularAttribute<Producto, String> descripcion;
    public static volatile SingularAttribute<Producto, Float> precio;
    public static volatile CollectionAttribute<Producto, Lineaorden> lineaordenCollection;
    public static volatile SingularAttribute<Producto, Integer> idProducto;

}