package paquete;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import paquete.LineaordenPK;
import paquete.Orden;
import paquete.Producto;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2014-11-21T23:59:20")
@StaticMetamodel(Lineaorden.class)
public class Lineaorden_ { 

    public static volatile SingularAttribute<Lineaorden, LineaordenPK> lineaordenPK;
    public static volatile SingularAttribute<Lineaorden, Integer> cantidad;
    public static volatile SingularAttribute<Lineaorden, Orden> orden;
    public static volatile SingularAttribute<Lineaorden, Producto> producto;

}